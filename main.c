/*
 *  ======== main.c ========
 */

#include <xdc/std.h>

#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/BIOS.h>

#include <ti/sysbios/knl/Task.h>

/*
 *  ======== taskFxn ========
 */
Void taskFxn1(UArg a0, UArg a1)
{
    System_printf("enter taskFxn1()\n");

    Task_sleep(1);

    System_printf("exit taskFxn1()\n");

    System_flush(); /* force SysMin output to console */
}

Void taskFxn2(UArg arg0, UArg arg1)
{
    System_printf("enter taskFxn2()\n");

    Task_sleep(10);

    System_printf("exit taskFxn2()\n");

    System_flush(); /* force SysMin output to console */
}

Void taskFxn3(UArg arg0, UArg arg1)
{
    System_printf("enter taskFxn3()\n");

    Task_sleep(100);

    System_printf("exit taskFxn3()\n");

    System_flush(); /* force SysMin output to console */
}

/*
 *  ======== main ========
 */
Int main()
{ 
    Task_Handle task;
    Error_Block eb;

    System_printf("enter main()\n");

    Error_init(&eb);
    task = Task_create(taskFxn1, NULL, &eb);
    task = Task_create(taskFxn2, NULL, &eb);
    task = Task_create(taskFxn3, NULL, &eb);
    if (task == NULL) {
        System_printf("Task_create() failed!\n");
        BIOS_exit(0);
    }

    BIOS_start();    /* does not return */
    return(0);
}
